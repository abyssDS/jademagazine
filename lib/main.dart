import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeWithSnap(),
    );
  }
}

class ActivityModel {
  String title;
  String lieu;

  ActivityModel({this.title, this.lieu});

  factory ActivityModel.fromJSON(Map<String, dynamic> json) {
    return ActivityModel(
      title: json["title"],
      lieu: json["lieu"],
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<ActivityModel> activities = [];

  @override
  void initState() {
    super.initState();
    Firestore.instance.collection("activities").getDocuments().then((value) {
      List<ActivityModel> _activities = [];
      for (var i = 0; i < value.documents.length; i++) {
        ActivityModel doc = ActivityModel(
          title: value.documents[i]["title"],
          lieu: value.documents[i]["lieu"],
        );

        _activities.add(doc);
      }
      setState(() {
        activities = _activities;
      });
    }).catchError((error) => print(error));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: activities
              .map((activity) => ListTile(
                    title: Text(activity.title),
                    subtitle: Text(activity.lieu),
                  ))
              .toList(),
        ),
      ),
    );
  }
}

class HomeWithSnap extends StatefulWidget {
  @override
  _HomeWithSnapState createState() => _HomeWithSnapState();
}

class _HomeWithSnapState extends State<HomeWithSnap> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          child: StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance.collection("activities").snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError)
                  return new Text('Error :${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator());
                  default:
                    return ListView(
                      children: snapshot.data.documents
                          .map((activity) =>
                              ListTile(title: Text(activity["title"])))
                          .toList(),
                    );
                }
              }),
        ));
  }
}
